# Changelog

## 1.0.0 - 2020-03-07

- Add tests.
- Bump up version to 1.0.0, hence making it ready for demonstration during
  class session.

## 0.1.0 - 2020-03-07

The original code before being refactored.
