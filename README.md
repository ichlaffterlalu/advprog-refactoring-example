# Refactoring Example

[![pipeline status](https://gitlab.cs.ui.ac.id/AdvProg/kki-2023/refactoring-example/badges/master/pipeline.svg)](https://gitlab.cs.ui.ac.id/AdvProg/kki-2023/refactoring-example/-/commits/master)
[![coverage report](https://gitlab.cs.ui.ac.id/AdvProg/kki-2023/refactoring-example/badges/master/coverage.svg)](https://gitlab.cs.ui.ac.id/AdvProg/kki-2023/refactoring-example/-/commits/master)

This project contains code examples from "Refactoring" by Martin Fowler. It is
used for demonstrating safe refactoring with tests.

## License

The initial base code and the refactored solution is based on the code found at
the chapter 1 of "Refactoring" by Martin Fowler. Code examples that deviated
from the book are created by Advanced Programming 2021 Teaching Team and licensed
under the [BSD-3 license](./LICENSE).
