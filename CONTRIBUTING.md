# How to Contribute

If you would like to contribute improvements such as new code or documentation,
you can do so by following [Forking Workflow]. Start by making a fork of this
project repository, then create a feature branch containing your contribution
in your own repository. Once you are ready, please make a Pull/Merge Request
and ensure the CI pipeline pass.

The project uses [Checkstyle] as code linter with configuration defined at
[`checkstyle.xml`](./checkstyle.xml). Please try to keep Checkstyle violations
as low as possible. Modern text editors and IDEs such as Visual Studio Code and
JetBrains IntelliJ provide Checkstyle plugin to automate the lint process.

Any questions, remarks, or suggestions should be posted to the project issue
tracker.

[Forking Workflow]: https://www.atlassian.com/git/tutorials/comparing-workflows/forking-workflow
[Checkstyle]: https://checkstyle.sourceforge.io/
