package csui.advprog2021.refactoringexample.domain;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

@DisplayName("A customer")
class CustomerTest {

    public static final String CUSTOMER_NAME = "Test";

    private Customer customer;

    @BeforeEach
    void setUp() {
        customer = new Customer(CUSTOMER_NAME);
    }

    @Test
    @DisplayName("should return name correctly")
    void testGetName() {
        assertEquals(CUSTOMER_NAME, customer.getName());
    }

    @Test
    @DisplayName("should have their name mentioned in the statement")
    void testStatement_shouldContain_customerName() {
        String statement = customer.statement();

        assertTrue(
            statement.contains(CUSTOMER_NAME),
            String.format("The name in the statement does not match. Actual: %s", statement)
        );
    }

    @Test
    @DisplayName("given no rentals, they should owe nothing")
    void testStatement_givenNoRentals_totalAmountIsZero() {
        String statement = customer.statement();

        verifyAmountInStatement(statement, 0);
    }

    @Test
    @DisplayName("given no rentals, their frequent renter points should be zero")
    void testStatement_givenNoRentals_frequentRenterPointsIsZero() {
        String statement = customer.statement();

        verifyFrequentRenterPointsInStatement(statement, 0);
    }

    @Test
    @DisplayName("given a regular movie rented for a day, their statement should be correct")
    void testStatement_givenRegularMovieAndRentedOneDay_produceCorrectStatement() {
        Movie movie = new Movie("A regular movie", Movie.REGULAR);
        Rental rental = new Rental(movie, 1);
        customer.addRental(rental);

        String statement = customer.statement();

        verifyAmountInStatement(statement, 2);
        verifyFrequentRenterPointsInStatement(statement, 1);
    }

    @Test
    @DisplayName("given a regular movie rented > 2 days, their statement should be correct")
    void testStatement_givenRegularMovieAndRentedMoreThanTwoDays_produceCorrectStatement() {
        Movie movie = new Movie("A regular movie", Movie.REGULAR);
        Rental rental = new Rental(movie, 3);
        customer.addRental(rental);

        String statement = customer.statement();

        verifyAmountInStatement(statement, 3.5);
    }

    @Test
    @DisplayName("given a new release movie rented for a day, their statement should be correct")
    void testStatement_givenNewReleaseMovieAndRentedOneDay_produceCorrectStatement() {
        Movie movie = new Movie("A new release movie", Movie.NEW_RELEASE);
        Rental rental = new Rental(movie, 1);
        customer.addRental(rental);

        String statement = customer.statement();

        verifyAmountInStatement(statement, 3);
    }

    @Test
    @DisplayName("given a new release movie rented > 2 days, their statement should be correct")
    void testStatement_givenNewReleaseMovieAndRentedMoreThanTwoDays_produceCorrectStatement() {
        Movie movie = new Movie("A new release movie", Movie.NEW_RELEASE);
        Rental rental = new Rental(movie, 3);
        customer.addRental(rental);

        String statement = customer.statement();

        verifyAmountInStatement(statement, 9);
        verifyFrequentRenterPointsInStatement(statement, 2);
    }

    @Test
    @DisplayName("given a children movie rented for a day, their statement should be correct")
    void testStatement_givenChildrenMovieAndRentedOneDay_produceCorrectStatement() {
        Movie movie = new Movie("A children movie", Movie.CHILDREN);
        Rental rental = new Rental(movie, 1);
        customer.addRental(rental);

        String statement = customer.statement();

        verifyAmountInStatement(statement, 1.5);
    }

    @Test
    @DisplayName("given a children movie rented > 3 days, their statement should be correct")
    void testStatement_givenChildrenMovieAndRentedMoreThanThreeDays_produceCorrectStatement() {
        Movie movie = new Movie("A children movie", Movie.CHILDREN);
        Rental rental = new Rental(movie, 4);
        customer.addRental(rental);

        String statement = customer.statement();

        verifyAmountInStatement(statement, 3);
    }

    private void verifyAmountInStatement(String actualStatement, double expectedAmount) {
        final double amount = 0 + expectedAmount;

        assertTrue(
            actualStatement.contains("Amount owed is " + amount),
            String.format(
                "Amount owned is not equal to %f. Actual statement: %s",
                amount, actualStatement
            )
        );
    }

    private void verifyFrequentRenterPointsInStatement(String actualStatement, int expectedPoints) {
        assertTrue(
            actualStatement.contains(
                String.format("You earned %d frequent renter points", expectedPoints)
            ),
            String.format(
                "Frequent renter points is not equal to %d. Actual statement: %s",
                expectedPoints, actualStatement
            )
        );
    }
}
