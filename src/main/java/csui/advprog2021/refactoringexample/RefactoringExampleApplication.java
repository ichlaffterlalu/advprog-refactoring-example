package csui.advprog2021.refactoringexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RefactoringExampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(RefactoringExampleApplication.class, args);
    }
}
