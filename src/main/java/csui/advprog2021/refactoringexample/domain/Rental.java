package csui.advprog2021.refactoringexample.domain;

/**
 * Represents a movie that currently being rented.
 *
 * The original code is taken from page 3 of "Refactoring" by Martin Fowler
 * (1999).
 */
public class Rental {

    private Movie movie;
    private int daysRented;

    public Rental(Movie movie, int daysRented) {
        this.movie = movie;
        this.daysRented = daysRented;
    }

    public Movie getMovie() {
        return movie;
    }

    public int getDaysRented() {
        return daysRented;
    }
}
